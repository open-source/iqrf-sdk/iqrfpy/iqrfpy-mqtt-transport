.PHONY: docs

build:
	python3 -m pip install --upgrade build
	python3 -m build

docs:
	rm -rf docs
	pdoc -d google -o docs mqtt_transport/

install:
	python3 -m pip install .

upload-test:
	python3 -m pip install --upgrade twine
	python3 -m twine upload --repository testpypi dist/*

upload:
	python3 -m pip install --upgrade twine
	python3 -m twine upload dist/*

clean:
	rm -rf build dist iqrfpy_mqtt_transport.egg-info .pytest_cache htmlcov docs
	find . -type d -name  "__pycache__" -exec rm -r {} +

codestyle:
	pycodestyle mqtt_transport
	pydocstyle mqtt_transport

lint:
	pylint mqtt_transport
